const { 
	AccountId,
	Client, 
	ContractCreateTransaction,
	ContractFunctionParameters,
	Hbar,
	PrivateKey
} = require("@hashgraph/sdk");
const utils = require("./hedera-utils.js");

async function main() {
	if (process.argv.length < 3) {
		console.log(`Pass in a bytecode ID obtained from 'file-to-hedera.js' to create a contract.`);
	}
	const bytecodeFileId = process.argv[2];
	const [client, operatorId, operatorPrivateKey] = await utils.connectToHedera();

	// Instantiate the smart contract
	const contractInstantiateTx = new ContractCreateTransaction()
		.setBytecodeFileId(bytecodeFileId)
		.setGas(100000)
		.setConstructorParameters(new ContractFunctionParameters().addString("Alice").addUint256(111111));
	const contractInstantiateSubmit = await contractInstantiateTx.execute(client);
	const contractInstantiateRx = await contractInstantiateSubmit.getReceipt(client);
	const contractId = contractInstantiateRx.contractId;
	const contractAddress = contractId.toSolidityAddress();
	console.log(`- The smart contract ID is: ${contractId}`);
	console.log(`- Smart contract ID in Solidity format: ${contractAddress}`);
}
main();