const { 
	AccountId,
	Client, 
	FileCreateTransaction,
	Hbar,
	PrivateKey
} = require("@hashgraph/sdk");
const utils = require("./hedera-utils.js");
const fs = require("fs");

async function main() {
	if (process.argv.length < 3) {
		console.log(`Pass in a file path to upload.`);
	}
	const filePath = process.argv[2];
 	const [client, operatorId, operatorPrivateKey] = await utils.connectToHedera();
	const file = fs.readFileSync(filePath);
	console.log(`- Uploading file at ${filePath} to Hedera...`);
	const fileCreateTx = new FileCreateTransaction()
		.setContents(file)
		.setKeys([operatorPrivateKey])
		.setMaxTransactionFee(new Hbar(0.75))
		.freezeWith(client);
	const fileCreateSign = await fileCreateTx.sign(operatorPrivateKey);
	const fileCreateSubmit = await fileCreateSign.execute(client);
	const fileCreateRx = await fileCreateSubmit.getReceipt(client);
	const fileId = fileCreateRx.fileId;
	console.log(`- The file ID is: ${fileId}`);
}
main();