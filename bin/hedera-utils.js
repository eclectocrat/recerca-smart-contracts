const { 
	AccountId,
	Client, 
	PrivateKey
} = require("@hashgraph/sdk");

require("dotenv").config();

const utils = {
	connectToHedera: async function () {
	    const operatorId = AccountId.fromString(process.env.OPERATOR_ID);
	    const operatorPrivateKey = PrivateKey.fromString(process.env.OPERATOR_PVKEY);
	    if (operatorId == null || operatorPrivateKey == null) {
	        throw new Error("Environment variables myAccountId and myPrivateKey must be present");
	    }
	    // TODO: Select target network
	    const client = Client.forTestnet();
		client.setOperator(operatorId, operatorPrivateKey);
		return [
			client, 
			operatorId, 
			operatorPrivateKey
		];
	}
}

module.exports = utils;