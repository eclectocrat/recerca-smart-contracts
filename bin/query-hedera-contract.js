const { 
	AccountId,
	Client, 
	ContractCallQuery,
	ContractCreateTransaction,
	ContractFunctionParameters,
	Hbar,
	PrivateKey
} = require("@hashgraph/sdk");
const utils = require("./hedera-utils.js");

async function main() {
	if (process.argv.length < 3) {
		console.log(`Pass in a contract ID obtained from 'create-hedera-contract.js' to query a contract.`);
	}
	const contractId = process.argv[2];
	const [client, operatorId, operatorPrivateKey] = await utils.connectToHedera();

	// Query the contract to check changes in state variable
	const contractQueryTx = new ContractCallQuery()
		.setContractId(contractId)
		.setGas(100000)
		.setFunction("getMobileNumber", new ContractFunctionParameters().addString("Alice"))
		.setMaxQueryPayment(new Hbar(0.00000001));
	const contractQuerySubmit = await contractQueryTx.execute(client);
	const contractQueryResult = contractQuerySubmit.getUint256(0);
	console.log(`- Here's the phone number you asked for: ${contractQueryResult}`);
}
main();