const { 
	AccountCreateTransaction,
	AccountId,
	Client, 
	Hbar,
	PrivateKey,
} = require("@hashgraph/sdk");
const utils = require("./hedera-utils.js");

async function main() {
	if (process.argv.length < 3) {
		console.log(`Pass in number of dummy accounts to create.`);
	}
	const numAccounts = process.argv[2];
	const [client, operatorId, operatorPrivateKey] = await utils.connectToHedera();

	const accounts = await Promise.all(Array.from({length: numAccounts}, async (_, index) => {
		const newKey = PrivateKey.generateED25519();
		let transaction = await new AccountCreateTransaction()
			.setKey(newKey.publicKey)
			.setInitialBalance(new Hbar(10))
			.execute(client);
		const receipt = await transaction.getReceipt(client);
		return {
			id: receipt.accountId.toString(),
			publicKey: newKey.publicKey.toString(),
			privateKey: newKey.toString()
		}
	}));

	console.log('[', accounts.map((account, index) => 
		`${JSON.stringify(account, null, 4)}`
	).join(','), ']');
}
main();