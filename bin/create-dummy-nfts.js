const { 
	AccountBalanceQuery,
	AccountId,
	AccountUpdateTransaction,
	Client, 
	ContractCreateTransaction,
	ContractFunctionParameters,
	CustomFixedFee,
	CustomRoyaltyFee,
	Hbar,
	PrivateKey,
	TokenCreateTransaction,
	TokenInfoQuery,
	TokenMintTransaction,
	TokenSupplyType,
	TokenType,
	TransferTransaction
} = require("@hashgraph/sdk");
const utils = require("./hedera-utils.js");
const fs = require('fs');

const supplyKey = PrivateKey.generate();
const adminKey = PrivateKey.generate();
const pauseKey = PrivateKey.generate();
const freezeKey = PrivateKey.generate();
const wipeKey = PrivateKey.generate();

// Randomly split `num`` discrete elements into `parts` bins.
function* splitNParts(num, parts) {
    let sumParts = 0;
    for (let i = 0; i < parts - 1; i++) {
        const pn = Math.ceil(Math.random() * (num - sumParts))
        yield pn
        sumParts += pn
    }
    yield num - sumParts;
}

async function main() {
	if (process.argv.length < 4) {
		console.error(`usage: node create-dummy-nfts.js N CONTRACT_NAME ACCOUNT_JSON
	- N: number of dummy NFT's to create
	- CONTRACT_ID: id of voting weight contract to set NFT weights for
	- ACCOUNT_JSON: file with account info created by running 'create-dummy-accounts.js'`);
		return;
	}
	const numNFTs = process.argv[2];
	const contract = process.argv[3];
	const accounts = JSON.parse(fs.readFileSync(process.argv[4]))
		.map(account => {
			return {
				id: AccountId.fromString(account.id),
				privateKey: PrivateKey.fromString(account.privateKey)
			};
		});
	const [client, operatorId, operatorPrivateKey] = await utils.connectToHedera();

	// Create NFTs
	let nftCustomFee = await new CustomRoyaltyFee()
        .setNumerator(5)
        .setDenominator(10)
        .setFeeCollectorAccountId(operatorId)
        .setFallbackFee(new CustomFixedFee().setHbarAmount(new Hbar(200)));

    let nftCreate = await new TokenCreateTransaction()
		.setTokenName("Dummy Voting Token")
		.setTokenSymbol("DVTK")
		.setTokenType(TokenType.NonFungibleUnique)
		.setDecimals(0)
		.setInitialSupply(0)
		.setTreasuryAccountId(operatorId)
		.setSupplyType(TokenSupplyType.Finite)
		.setMaxSupply(numNFTs)
		.setCustomFees([nftCustomFee])
		.setAdminKey(adminKey)
		.setSupplyKey(supplyKey)
		// ???
		// .setPauseKey(pauseKey)
		.setFreezeKey(freezeKey)
		.setWipeKey(wipeKey)
		.freezeWith(client)
		.sign(operatorPrivateKey);

	let nftCreateTxSign = await nftCreate.sign(adminKey);
	let nftCreateSubmit = await nftCreateTxSign.execute(client);
	let nftCreateRx = await nftCreateSubmit.getReceipt(client);
	let tokenId = nftCreateRx.tokenId;
	console.log(`Created NFT with Token ID: ${tokenId}\n`);

	var tokenInfo = await new TokenInfoQuery().setTokenId(tokenId).execute(client);
	console.table(tokenInfo.customFees[0]);

	nfts = [];
	for (var i = 0; i < numNFTs; i++) {
		nfts[i] = await mintToken("dummy metadata");
		console.log(`Created NFT ${tokenId} with serial: ${nfts[i].serials[0].low}`);
	}

	var tokenInfo = await new TokenInfoQuery().setTokenId(tokenId).execute(client);
	console.log(`Current NFT supply: ${tokenInfo.totalSupply} \n`);

	// Auto associate for each account.
	await Promise.all(accounts.map(async (account) => {
		let associateTx = await new AccountUpdateTransaction()
			.setAccountId(account.id)
			.setMaxAutomaticTokenAssociations(100)
			.freezeWith(client)
			.sign(account.privateKey);
		let associateTxSubmit = await associateTx.execute(client);
		let associateRx = await associateTxSubmit.getReceipt(client);
		console.log(`NFT Auto-Association: ${associateRx.status}`);

		balance = await checkBalance(account.id);
		console.log(`- ${account.id} balance: ${balance[0] ? balance[0] : 0} NFTs of ID:${tokenId} and ${balance[1]}`);
	}));

	// Register NFT id's with contract.
	nftIds.forEach(nftId => {
		
	});

	// Distribute NFT's randomly among accounts.
	const nftsDividedByAccounts = [...splitNParts(numNFTs, accounts.length)];
	await Promise.all(nftsDividedByAccounts.map(async (num, index) => {
		const account = accounts[index];
		let tokenTransferTx = new TransferTransaction();
		for (let i=0; i<num; i++) {
			const nft = nfts.pop();
			tokenTransferTx.addNftTransfer(tokenId, nft.serials[0].low, operatorId, account.id)
		};
		await tokenTransferTx
			.freezeWith(client)
			.sign(operatorPrivateKey);
		let tokenTransferSubmit = await tokenTransferTx.execute(client);
		let tokenTransferRx = await tokenTransferSubmit.getReceipt(client);
		console.log(`- NFT transfer to ${account.id.toString()} status: ${tokenTransferRx.status}`);

		balance = await checkBalance(account.id);
		console.log(`- ${account.id} balance: ${balance[0] ? balance[0] : 0} NFTs of ID:${tokenId} and ${balance[1]}`);
	}));

	// Closure Functions
	async function mintToken(metadata) {
		mintTx = await new TokenMintTransaction()
			.setTokenId(tokenId)
			.setMetadata([Buffer.from(metadata)])
			.freezeWith(client);
		let mintTxSign = await mintTx.sign(supplyKey);
		let mintTxSubmit = await mintTxSign.execute(client);
		let mintRx = await mintTxSubmit.getReceipt(client);
		return mintRx;
	}

	async function checkBalance(id) {
		balanceCheckTx = await new AccountBalanceQuery()
			.setAccountId(id)
			.execute(client);
		return [balanceCheckTx.tokens._map.get(tokenId.toString()), balanceCheckTx.hbars];
	}
}
main();