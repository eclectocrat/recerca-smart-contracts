// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

pragma experimental SMTChecker;

contract LookupContract {
    mapping (address => uint) public contractDirectory;

    constructor (address memory _addr, uint _factor) public {
        contractDirectory[_addr] = _factor;
    }

    function setContract(address memory _addr, uint _factor) public {
        contractDirectory[_addr] = _factor;
    }

    function getContract(string memory _addr) public view returns (uint) {
        return contractDirectory[_addr];
    }
}

contract VoteCollectorContract {
    mapping (address => uint) public votes;

    constructor (address[] memory _options, uint _defaultValue) {
        for (uint i=0; i < _options.length; i++) {
            votes[_options[i]] = _defaultValue;
        }
    }
}