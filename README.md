# Simple Hedera smart contract 2.0

### About
Super simple and primitive smart contract package for Hedera Smart Contracts 2.0.

This package has been evolved from the official tutorial:
- https://hedera.com/blog/how-to-deploy-smart-contracts-on-hedera-part-1-a-simple-getter-and-setter-contract
- https://github.com/ed-marquez/hedera-example-contract-getter-setter/blob/master/index.js

### Setup
- Create a plaintext file `.env` in the project root with the following contents:
	```
	OPERATOR_ID = _your testnet account address_
	OPERATOR_PBKEY = _your testnet account public key_
	OPERATOR_PVKEY = _your testnet account private key_
	```

#### Create and deploy your smart contract

- Code your contract in the `contracts` folder.
- Run `solcjs --bin contracts/YourContract.sol`
- Run `node bin/file-to-hedera.js YourContract_sol_YourContract.bin` and save the `<file ID>`.
- Modify `bin/create-hedera-contract.js` to call the constructor of your contract with the desired parameters.
- Run `node bin/create-hedera-contract.js <file ID>` and save the `<smart contract ID>` and `<smart contract solidity ID>`.

#### Create dummy data to test your contract

- Run `node bin/create-dummy-accounts.js X > dummy-accounts.json` to create X testnet accounts.
- Run `node bin/create-dummy-ntfs.js X dummy-accounts.json YourContractName` to randomly distribute some voting NFTs to the dummy accounts and register them with random weights to the smart contract.

### License
Copyright 2022 Jeremy Jurksztowicz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.